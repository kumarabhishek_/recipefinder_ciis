
package com.example.recipe_finder.ui.recipelist;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.recipe_finder.model.Recipe;

import java.util.List;
import java.util.Objects;

public class RecipeListViewModel extends AndroidViewModel {

    private final LiveData<List<Recipe>> recipes;
    private int selected;
    RecipeRepository repository;


    public RecipeListViewModel(@NonNull Application application) {
        super(application);

        repository = new RecipeRepository(application);
        recipes = repository.getRecipes();

    }

    public void loadRecipes(String ingredients, ApiResponseListener listener) { repository.loadRecipes(ingredients, listener);
    }

    public LiveData<List<Recipe>> getRecipes() {
        return recipes;
    }

    public void setSelected(int index) {selected = index; }

    public Recipe getSelectedRecipe() {
        return Objects.requireNonNull(recipes.getValue()).get(selected);
    }

}
