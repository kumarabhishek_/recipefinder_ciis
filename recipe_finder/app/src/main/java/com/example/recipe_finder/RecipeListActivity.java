package com.example.recipe_finder;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.recipe_finder.ui.recipelist.RecipeListFragment;

public class RecipeListActivity extends AppCompatActivity  {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_list);

        Bundle bundle = new Bundle();
        bundle.putString("ingredients", getIntent().getStringExtra("ingredients"));
        RecipeListFragment fragment = new RecipeListFragment();
        fragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.recipe_fragment_container, fragment)
                .commit();

    }


}