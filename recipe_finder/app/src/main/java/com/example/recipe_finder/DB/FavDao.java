package com.example.recipe_finder.DB;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface FavDao {

    @Insert
    public void addData(FavListItem favListItem);

    @Query("select * from favourites_list")
    public List<FavListItem> getFavData();

    @Query("SELECT EXISTS (SELECT 1 FROM favourites_list WHERE id=:id)")
    public int isFavorite(int id);

    @Delete
    public void delete(FavListItem favListItem);
}
