package com.example.recipe_finder.imageclassifier_api.api_utils;

import com.example.recipe_finder.imageclassifier_api.datamodels.Logmeal;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface LogmealAPIService {
    String APIKey = "78f7d9fbc2d7eb1e2a48b6c981c526e611b78ea7";

    @Headers("Authorization: Bearer "+APIKey)
    @Multipart
    @POST("/v2/recognition/dish")
    Call<Logmeal> uploadImage(
            @Part MultipartBody.Part image
    );
}
