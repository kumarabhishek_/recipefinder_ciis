package com.example.recipe_finder.ui.Favourites;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recipe_finder.R;
import com.example.recipe_finder.model.Recipe;
import com.example.recipe_finder.ui.recipelist.InstructionsAdapter;
import com.example.recipe_finder.ui.recipelist.RecipeIngredientsAdapter;
import com.squareup.picasso.Picasso;

public class FavouriteRecipeFragment extends Fragment {
    private ImageView recipeImage;
    private TextView recipeTitle;
    private TextView recipeTime;
    private EditText servings;
    private TextView credit;
    private CheckBox click_favourite_button;
    private RecyclerView instructionsListView;
    private RecyclerView ingredientsListView;
    private Recipe selectedRecipe;
    // private RecipeListViewModel viewModel;

    public FavouriteRecipeFragment(Recipe selectedRecipe) {
         this.selectedRecipe = selectedRecipe;
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recipe, container, false);
        Bundle bundle = getArguments();
        // selectedRecipe = bundle.getParcelable("recipe");

        recipeImage = view.findViewById(R.id.recipe_image);
        recipeTitle = view.findViewById(R.id.recipe_title);
        click_favourite_button = view.findViewById(R.id.click_favourite_checkbox);
        recipeTime = view.findViewById(R.id.recipe_time);
        servings = view.findViewById(R.id.recipe_servings_amount);
        credit = view.findViewById(R.id.recipe_credit);
        click_favourite_button = view.findViewById(R.id.click_favourite_checkbox);
        click_favourite_button.setChecked(true);
        click_favourite_button.setEnabled(false);

        // viewModel = new ViewModelProvider(requireActivity()).get(RecipeListViewModel.class);

        // Recipe selectedRecipe = viewModel.getSelectedRecipe();

        recipeTitle.setText(selectedRecipe.getTitle());
        Picasso.get().load(selectedRecipe.getImage()).into(recipeImage);
        String recipeTimeText = "Ready in " + selectedRecipe.getReadyInMinutes() + " Minutes";
        recipeTime.setText(recipeTimeText);
        servings.setText(String.valueOf(selectedRecipe.getServings()));
        String recipeCreditText = "Click here to see the original recipe on " + selectedRecipe.getCreditsText();
        credit.setText(recipeCreditText);

        instructionsListView = view.findViewById(R.id.recipe_instructions_list);
        final InstructionsAdapter instructionsAdapter = new InstructionsAdapter(getContext(), selectedRecipe.getAnalyzedInstructions().get(0).getSteps());
        instructionsListView.setLayoutManager(new LinearLayoutManager(getContext()));
        instructionsListView.setAdapter(instructionsAdapter);
        instructionsListView.setNestedScrollingEnabled(false);

        ingredientsListView = view.findViewById(R.id.recipe_ingredients_list);
        final RecipeIngredientsAdapter ingredientsAdapter = new RecipeIngredientsAdapter(getContext(), selectedRecipe.getExtendedIngredients());
        ingredientsListView.setLayoutManager(new LinearLayoutManager(getContext()));
        ingredientsListView.setAdapter(ingredientsAdapter);
        ingredientsListView.setNestedScrollingEnabled(false);

        servings.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() > 0)
                    ingredientsAdapter.updateIngredientAmount(selectedRecipe.getServings(), Float.parseFloat(s.toString()));
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //link original recipe
        credit.setMovementMethod(LinkMovementMethod.getInstance());
        credit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(selectedRecipe.getSourceUrl()));
                startActivity(intent);
            }
        });

        return view;
    }

}
