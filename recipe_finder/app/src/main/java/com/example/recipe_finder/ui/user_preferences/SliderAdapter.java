package com.example.recipe_finder.ui.user_preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.PagerAdapter;

import com.example.recipe_finder.R;

import java.util.ArrayList;
import java.util.List;

public class SliderAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;
    ImageView imageView;
    TextView textView;
    Display display;
    List<Checkbox_Item> checkbox_items;

    public SliderAdapter(Context context, Display display) {
        this.context = context;
        this.display = display;
    }

    int[] images = {R.drawable.cuisines,
                    R.drawable.food_intolerences,
                    R.drawable.diet_types};

    int[] questions = {R.string.onboarding_2, R.string.onboarding_3, R.string.onboarding_4};

    String [] page_one_options = {"African", "American", "British", "Cajun",
                                "Caribbean", "Chinese", "Eastern European",
                                "European", "French", "German", "Greek", "Indian",
                                "Irish", "Italian", "Japanese", "Jewish", "Korean",
                                "Latin American", "Mediterranean", "Mexican",
                                "Middle Eastern", "Nordic", "Southern", "Spanish",
                                "Thai", "Vietnamese"};

    String [] page_two_options = {"Dairy", "Egg", "Gluten", "Grain", "Peanut", "Seafood",
                                "Sesame", "Shellfish", "Soy", "Sulfite", "Tree Nut", "Wheat"};

    String [] page_three_options = {"Gluten Free", "Ketogenic", "Vegetarian", "Lacto-Vegetarian",
                                    "Ovo-Vegetarian", "Vegan", "Pescetarian", "Paleo", "Primal",
                                    "Whole30"};



    @Override
    public int getCount() {
        return questions.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) { // Use if position to inflate different types of layouts in future.
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.onboarding_slides_layout, container, false);
        imageView = view.findViewById(R.id.slide_image);
        imageView.getLayoutParams().height = display.getHeight()/4;
        textView = view.findViewById(R.id.questions);
        ListView list_view = view.findViewById(R.id.list_checkboxes);

        SharedPreferences sharedPreferences = context.getSharedPreferences("Selections", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("selection_" + position, null);
        editor.apply();

        if(position == 0)
        {
            checkbox_items = new ArrayList<>();
            for (String page_one_option : page_one_options) {

                Checkbox_Item checkbox_item = new Checkbox_Item(page_one_option);
                checkbox_items.add(checkbox_item);
            }
        }

        if(position == 1)
        {
            checkbox_items = new ArrayList<>();

            for (String page_two_option : page_two_options) {

                Checkbox_Item checkbox_item = new Checkbox_Item(page_two_option);
                checkbox_items.add(checkbox_item);
            }
        }

        if(position == 2)
        {
            checkbox_items = new ArrayList<>();

            for (String page_three_option : page_three_options) {

                Checkbox_Item checkbox_item = new Checkbox_Item(page_three_option);
                checkbox_items.add(checkbox_item);
            }
        }

        imageView.setImageResource(images[position]);
        textView.setText(questions[position]);

        CheckboxListAdapter adapter = new CheckboxListAdapter(context,
                R.layout.preferences_checkboxes,
                checkbox_items,
                position);
        list_view.setAdapter(adapter);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }


}
