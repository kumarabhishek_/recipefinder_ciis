package com.example.recipe_finder.ui.Favourites;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recipe_finder.DB.FavListItem;
import com.example.recipe_finder.MainActivity;
import com.example.recipe_finder.R;
import com.example.recipe_finder.api.ApiService;
import com.example.recipe_finder.api.RetrofitClient;
import com.example.recipe_finder.model.Recipe;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class FavouritesFragment extends Fragment implements FavAdapter.ItemClickListener {

    private FavAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    ConstraintLayout constraintLayout;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_recipe_list, container, false);

        RecyclerView recyclerView = root.findViewById(R.id.recipe_list);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        List<FavListItem> favListItems = MainActivity.appDB.favDao().getFavData();
        adapter = new FavAdapter(favListItems, getContext());
        adapter.setItemClickListener(this);
        recyclerView.setAdapter(adapter);
        constraintLayout = root.findViewById(R.id.recipe_list_container);


        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                MainActivity.appDB.favDao().delete(favListItems.get(viewHolder.getAdapterPosition()));
                favListItems.remove(viewHolder.getAdapterPosition());
                adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
            }
        }).attachToRecyclerView(recyclerView);

        return root;
    }


    @Override
    public void onItemClicked(FavListItem favListItem) {

        String apiKey = requireContext().getResources().getString(R.string.api_key);
        Retrofit retrofit = RetrofitClient.getInstance();
        ApiService apiService = retrofit.create(ApiService.class);
        Call<Recipe> call = apiService.getRecipeInformation((int) favListItem.getId(), apiKey);

        call.enqueue(new Callback<Recipe>() {

            @Override
            public void onResponse(Call<Recipe> call, Response<Recipe> response) {
                Toast toast = Toast.makeText(getContext(), response.body().getTitle(), Toast.LENGTH_SHORT);
                toast.show();
                FavouriteRecipeFragment favouriteRecipeFragment = new FavouriteRecipeFragment(response.body());
                constraintLayout.removeAllViews();
                getActivity()
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.recipe_list_container, favouriteRecipeFragment)
                        .addToBackStack(null)
                        .commit();
            }

            @Override
            public void onFailure(Call<Recipe> call, Throwable t) {

            }
        });
    }
}