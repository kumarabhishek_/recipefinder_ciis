package com.example.recipe_finder.ui.recipelist;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.recipe_finder.R;
import com.example.recipe_finder.api.ApiService;
import com.example.recipe_finder.api.RetrofitClient;
import com.example.recipe_finder.model.Recipe;
import com.example.recipe_finder.model.Result;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;




public class RecipeRepository {

    private final String apiKey;
    private static final String TYPE = null;
    private static final boolean INSTRUCTIONS_REQUIRED = true;
    private static final boolean FILL_INGREDIENTS = true;
    private static final boolean ADD_RECIPE_INFORMATION = true;
    private static final boolean IGNORE_PANTRY = true;
    private static final String SORT = "min-missing-ingredients";
    private static final String SORT_DIRECTION = "desc";
    private static final boolean LIMIT_LICENSE = true;
    private static final int NUMBER = 10;

    private MutableLiveData<List<Recipe>> recipes;
    private Retrofit retrofit;
    private ApiService apiService;

    private String cuisine;
    private String intolerances;
    private String diet;


    public RecipeRepository(Application application) {

        retrofit = RetrofitClient.getInstance();
        apiService = retrofit.create(ApiService.class);
        recipes = new MutableLiveData<>();

        apiKey = application.getResources().getString(R.string.api_key);

        SharedPreferences sharedPreferences = application.getSharedPreferences("Selections", Context.MODE_PRIVATE);
        cuisine = sharedPreferences.getString("selection_0",null);
        intolerances = sharedPreferences.getString("selection_1",null);
        diet = sharedPreferences.getString("selection_2",null);

    }

    //gets recipes from the spoonacular api
    public void loadRecipes(String ingredients, ApiResponseListener listener) {
        Call<Result> call = apiService.getRecipes(cuisine, diet, intolerances,
                ingredients,TYPE,INSTRUCTIONS_REQUIRED,FILL_INGREDIENTS,
                ADD_RECIPE_INFORMATION,IGNORE_PANTRY,SORT,SORT_DIRECTION,0,
                NUMBER,LIMIT_LICENSE,apiKey);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if(response.isSuccessful()){
                    if(!response.body().getRecipes().isEmpty()) {
                        recipes.setValue(response.body().getRecipes());
                    } else {
                        listener.apiResponseFailure("No fitting recipes have been found. Please try again with less ingredients or different preferences.");
                    }
                } else if(response.code() == 402) {
                    listener.apiResponseFailure("No more Api Calls left for today. Try again tomorrow");
                } else {
                    listener.apiResponseFailure("An unexpected Error occurred");
                }

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                listener.apiResponseFailure("An unexpected Error occurred");
            }
        });
    }

    public LiveData<List<Recipe>> getRecipes() {
        return recipes;
    }

}
