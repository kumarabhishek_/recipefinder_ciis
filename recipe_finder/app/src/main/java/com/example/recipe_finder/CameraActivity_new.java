package com.example.recipe_finder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.recipe_finder.imageclassifier_api.api_utils.APIUtils;
import com.example.recipe_finder.imageclassifier_api.api_utils.LogmealAPIService;
import com.example.recipe_finder.imageclassifier_api.datamodels.Logmeal;
import com.example.recipe_finder.imageclassifier_api.datamodels.RecognitionResult;
import com.example.recipe_finder.ml.Model;
import com.example.recipe_finder.ui.home.HomeFragment;

import org.tensorflow.lite.support.image.TensorImage;
import org.tensorflow.lite.support.label.Category;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CameraActivity_new extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private CameraPreview mPreview;
    private Camera.PictureCallback mPicture;
    private ImageView capture;
    private Context myContext;
    private SurfaceView cameraPreview;
    private ConstraintLayout surfaceViewContainer;
    public static Bitmap bitmap;
    int request_code = 999;
    private static final String[] CAMERA_PERMISSION = new String[]{Manifest.permission.CAMERA};
    private static final int CAMERA_REQUEST_CODE = 10;

    private LogmealAPIService apiService; // For Logmeal API
    List<RecognitionResult> itemLabels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_new);

        myContext = this;
        cameraPreview = (SurfaceView) findViewById(R.id.camera_preview);
        capture = findViewById(R.id.capture_image);

    }

    private boolean hasCameraPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, CAMERA_PERMISSION, CAMERA_REQUEST_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            startCamera();
        }
        else
            finish();
    }

    private void startCamera() {
        mPreview = new CameraPreview(myContext, (SurfaceView) cameraPreview);
        mPreview.setCamera();
        ((ConstraintLayout) findViewById(R.id.camera_container)).addView(mPreview);
        mPicture = getPictureCallback();

        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPreview.mCamera.autoFocus(new Camera.AutoFocusCallback() {
                    @Override
                    public void onAutoFocus(boolean success, Camera camera) {
                        if(success){
                            // camera.stopPreview();
                            camera.takePicture(null, null, mPicture);
                        }
                    }
                });
            }
        });

    }


    @Override
    protected void onPause() {
        super.onPause();
        if (mPreview != null) {
            mPreview.releaseCamera();
        }
    }

    public void onResume() {
        super.onResume();
        // surfaceViewContainer.removeAllViews();

        if (hasCameraPermission()) {
            startCamera();
        } else {
            requestPermission();
        }


    }


    private Camera.PictureCallback getPictureCallback() {
        String file_name = "Latest_photo";
        Camera.PictureCallback picture = new Camera.PictureCallback() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                camera.stopPreview();
                bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                bitmap = rotateImage(bitmap, 90);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                FileOutputStream fo = null;
                try
                {
                    fo = openFileOutput(file_name, Context.MODE_PRIVATE);
                    fo.write(bytes.toByteArray());
                    fo.close();

                    // Converting the image into JPEG format and saving it for Logmeal API
                    String path = getApplicationContext().getExternalFilesDir(null).toString();
                    Log.v("Photosaving", path);
                    File f = new File(path, "latest_foodItem.jpg");
                    FileOutputStream fos = new FileOutputStream(f);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                    fos.flush();
                    fos.close();

                    try {
                        Model model = Model.newInstance(getApplicationContext());

                        // Creates inputs for reference.
                        TensorImage input = TensorImage.fromBitmap(bitmap);

                        // Runs model inference and gets result.
                        Model.Outputs outputs = model.process(input);
                        List<Category> probability = outputs.getProbabilityAsCategoryList();

                        probability.sort(Comparator.comparing(Category::getScore));

                        Intent intent = new Intent(CameraActivity_new.this, SelectLabels.class);

                        //Logmeal API call
                        apiService = APIUtils.getAPIService();
                        RequestBody requestFile =
                                RequestBody.create(
                                        MediaType.parse("multipart/form-data"),
                                        f
                                );
                        MultipartBody.Part body =
                                MultipartBody.Part.createFormData("image", f.getName(), requestFile);
                        Call<Logmeal> call = apiService.uploadImage(body);
                        Toast.makeText(getApplicationContext(), "Waiting for prediction", Toast.LENGTH_LONG).show();
                        call.enqueue(new Callback<Logmeal>() {
                            @Override
                            public void onResponse(Call<Logmeal> call,
                                                   Response<Logmeal> response) {

                                //If API call is successful, label_1 is from Logmeal and rest from machine learning model
                                if(response.code() == 200 && response.body().getRecognitionResults() != null) {
                                    itemLabels = response.body().getRecognitionResults();
                                    Log.v("Logmeal Upload", "Upload success. Predictions: "+response.body().getRecognitionResults().get(0).getName());
                                    intent.putExtra("label_1", itemLabels.get(0).getName());
                                }
                                else {
                                    Log.v("Logmeal Upload", "Upload success, but error with prediction" );
                                    intent.putExtra("label_1", probability.get(probability.size()-4).getLabel());
                                }

                                //Rest of the item labels comes from the model
                                intent.putExtra("label_2", probability.get(probability.size()-1).getLabel());
                                intent.putExtra("label_3", probability.get(probability.size()-2).getLabel());
                                intent.putExtra("label_4", probability.get(probability.size()-3).getLabel());
                                startActivityForResult(intent, request_code);

                                // Releases model resources if no longer used.
                                model.close();
                            }

                            @Override
                            public void onFailure(Call<Logmeal> call, Throwable t) {
                                Log.e("Logmeal Upload error:", t.getMessage());

                                //In case of connection failure, we take prediction from the models
                                intent.putExtra("label_1", probability.get(probability.size()-1).getLabel());
                                intent.putExtra("label_2", probability.get(probability.size()-2).getLabel());
                                intent.putExtra("label_3", probability.get(probability.size()-3).getLabel());
                                intent.putExtra("label_4", probability.get(probability.size()-4).getLabel());
                                startActivityForResult(intent, request_code);

                                // Releases model resources if no longer used.
                                model.close();
                            }
                        });
                        //End of Logmeal

                    } catch (Exception e) {
                        // TODO Handle the exception
                    }
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        };
        return picture;
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Intent intent = new Intent(this, HomeFragment.class);
            intent.putExtra("Label", data.getStringExtra("Final_label"));
            setResult(RESULT_OK, intent);
            finish();
        }

    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

}