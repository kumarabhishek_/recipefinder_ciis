package com.example.recipe_finder.imageclassifier_api.datamodels;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecognitionResult {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("prob")
    @Expose
    private Double prob;
    @SerializedName("subclasses")
    @Expose
    private List<Object> subclasses = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getProb() {
        return prob;
    }

    public void setProb(Double prob) {
        this.prob = prob;
    }

    public List<Object> getSubclasses() {
        return subclasses;
    }

    public void setSubclasses(List<Object> subclasses) {
        this.subclasses = subclasses;
    }

}