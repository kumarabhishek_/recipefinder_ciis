package com.example.recipe_finder.ui.user_preferences;

import org.jetbrains.annotations.NotNull;

public class Checkbox_Item {

    private final String checkbox_text;

    public Checkbox_Item(String checkbox_text) {
        this.checkbox_text = checkbox_text;
    }

    @NotNull
    @Override
    public String toString() {
        return checkbox_text;
    }

    public String getCheckbox_text() {
        return checkbox_text;
    }



}