package com.example.recipe_finder.api;

import com.example.recipe_finder.model.Recipe;
import com.example.recipe_finder.model.Result;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("recipes/complexSearch")
    Call<Result> getRecipes(@Query("cuisine") String cuisine,
                            @Query("diet") String diet,
                            @Query("intolerances") String intolerances,
                            @Query("includeIngredients") String includeIngredients,
                            @Query("type") String type,
                            @Query("instructionsRequired") boolean instructionsRequired,
                            @Query("fillIngredients") boolean fillIngredients,
                            @Query("addRecipeInformation") boolean addRecipeInformation,
                            @Query("ignorePantry") boolean ignorePantry,
                            @Query("sort") String sort,
                            @Query("sortDirection") String sortDirection,
                            @Query("offset") int offset,
                            @Query("number") int number,
                            @Query("limitLicense") boolean limitLicense,
                            @Query("apiKey") String apiKey);

    @GET("recipes/{id}/information")
    Call<Recipe> getRecipeInformation(@Path("id") int id,
                                      @Query("apiKey") String apiKey);
}
