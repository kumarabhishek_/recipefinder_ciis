package com.example.recipe_finder.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class HomeViewModel extends ViewModel {

    private MutableLiveData<String> ingredients_list;
    private MutableLiveData<Boolean> ingredients_checkbox_list;

    public HomeViewModel() {
        ingredients_list = new MutableLiveData<>();
        ingredients_list.setValue("This is home fragment");
    }

    public LiveData<String> getText() {
        return ingredients_list;
    }
}