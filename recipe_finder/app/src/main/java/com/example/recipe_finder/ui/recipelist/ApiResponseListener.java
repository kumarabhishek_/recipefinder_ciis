package com.example.recipe_finder.ui.recipelist;

public interface ApiResponseListener {
    public void apiResponseFailure(String message);
}
