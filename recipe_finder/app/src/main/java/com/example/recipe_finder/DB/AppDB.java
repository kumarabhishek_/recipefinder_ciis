package com.example.recipe_finder.DB;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {FavListItem.class, IngredientsListItem.class}, version = 1)
public abstract class AppDB extends RoomDatabase {

    public abstract FavDao favDao();

    public abstract Ingredients_List_Dao ingredients_list_dao();

}
