package com.example.recipe_finder.ui.recipelist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recipe_finder.R;
import com.example.recipe_finder.model.Recipe;

import java.util.List;

public class RecipeListFragment extends Fragment implements RecipeListAdapter.ItemClickListener, ApiResponseListener{

    private RecyclerView recipeListView;
    private RecipeListViewModel viewModel;


    public RecipeListFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String ingredients = null;
        Bundle bundle = this.getArguments();
        if(bundle != null) {
            ingredients = bundle.getString("ingredients", null);
        }

        viewModel = new ViewModelProvider(requireActivity()).get(RecipeListViewModel.class);
        viewModel.loadRecipes(ingredients, this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_recipe_list, container, false);


        recipeListView = view.findViewById(R.id.recipe_list);
        final RecipeListAdapter adapter = new RecipeListAdapter(getActivity());
        adapter.setItemClickListener(this);
        recipeListView.setAdapter(adapter);
        recipeListView.setLayoutManager(new LinearLayoutManager(getActivity()));

        viewModel.getRecipes().observe(requireActivity(), recipes -> {
            adapter.setRecipes(recipes);
        });

        return view;
    }

    @Override
    public void onItemClicked(int index) {

        viewModel.setSelected(index);
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.recipe_fragment_container, new RecipeFragment())
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void apiResponseFailure(String message) {
        Toast.makeText(getContext(),message,Toast.LENGTH_LONG).show();
        getActivity().finish();
    }
}