package com.example.recipe_finder.ui.home;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recipe_finder.DB.IngredientsListItem;
import com.example.recipe_finder.MainActivity;
import com.example.recipe_finder.R;

import java.util.List;

public class IngredientListAdapter extends RecyclerView.Adapter<IngredientListAdapter.ViewHolder>{

    private List<IngredientsListItem> ingredientsListItems;
    Context context;

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView ingredient_textview;
        public AppCompatCheckBox ingredient_check;

        public ViewHolder(View v)
        {
            super(v);
            ingredient_textview = v.findViewById(R.id.ingredients_text);
            ingredient_check = v.findViewById(R.id.ingredients_check);
        }
    }

    public IngredientListAdapter(List<IngredientsListItem> ingredientsListItems, Context context){
        this.ingredientsListItems = ingredientsListItems;
        this.context = context;
    }

    @NonNull
    @Override
    public IngredientListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.ingredients_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull IngredientListAdapter.ViewHolder holder, int position) {
        IngredientsListItem ingredientsListItem = ingredientsListItems.get(position);

        String name = ingredientsListItem.getIngredient_name();
        boolean is_checked = ingredientsListItem.isIs_checked();
        holder.ingredient_textview.setText(name);
        holder.ingredient_check.setChecked(is_checked);
        holder.ingredient_textview.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {

                  LayoutInflater dialog_inflater = LayoutInflater.from(v.getContext());
                  View write_dialog = dialog_inflater.inflate(R.layout.write_dialog, null);
                  AlertDialog.Builder write_dialog_builder = new AlertDialog.Builder(v.getContext(), R.style.AlertDialogCustom);
                  write_dialog_builder.setView(write_dialog);
                  TextView dialog_title = write_dialog.findViewById(R.id.write_dialog_title);
                  dialog_title.setText(R.string.edit_ingredient_dialog);

                  final EditText userInputDialogEditText = write_dialog.findViewById(R.id.write_dialog_edittext);
                  userInputDialogEditText.setText(name);
                  write_dialog_builder.setCancelable(false).setPositiveButton("Update",
                          new DialogInterface.OnClickListener() {
                              public void onClick(DialogInterface dialogBox, int id) {
                              }
                          })
                          .setNegativeButton("Cancel",
                                  new DialogInterface.OnClickListener() {
                                      public void onClick(DialogInterface dialogBox, int id) {
                                          dialogBox.cancel();
                                      }
                                  });

                  AlertDialog alertDialogAndroid = write_dialog_builder.create();
                  alertDialogAndroid.show();
                  alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                      @Override
                      public void onClick(View v) {
                          if(MainActivity.appDB.ingredients_list_dao().count(userInputDialogEditText.getText().toString()) > 0)
                              userInputDialogEditText.setError("This already exists in the list. Click \"Cancel\" to go back");
                          else {
                              MainActivity.appDB.ingredients_list_dao().delete(ingredientsListItem);
                              IngredientsListItem new_ingredient = new IngredientsListItem();
                              new_ingredient.setIngredient_name(userInputDialogEditText.getText().toString());
                              new_ingredient.setIs_checked(true);
                              ingredientsListItems.set(position, new_ingredient);
                              MainActivity.appDB.ingredients_list_dao().addData(new_ingredient);
                              notifyItemChanged(position);
                              alertDialogAndroid.dismiss();
                          }
                      }
                  });
              }
          });
        holder.ingredient_check.setOnClickListener(v -> ingredientsListItem.setIs_checked(holder.ingredient_check.isChecked()));


    }

    @Override
    public int getItemCount() {
        return ingredientsListItems.size();
    }
}