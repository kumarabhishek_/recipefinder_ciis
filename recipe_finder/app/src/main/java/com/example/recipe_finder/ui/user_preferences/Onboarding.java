package com.example.recipe_finder.ui.user_preferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.recipe_finder.MainActivity;
import com.example.recipe_finder.R;

public class Onboarding extends AppCompatActivity {

    ViewPager viewPager;
    LinearLayout linearLayout;
    ImageView zero, one, two;
    ImageView[] imageViews;
    Button button;
    SliderAdapter pagerAdapter;
    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Display display = getWindowManager().getDefaultDisplay();

        setContentView(R.layout.activity_onboarding);

        viewPager = findViewById(R.id.slider);
        linearLayout = findViewById(R.id.dots);
        button = findViewById(R.id.next_btn);
        zero = findViewById(R.id.intro_indicator_1);
        one = findViewById(R.id.intro_indicator_2);
        two = findViewById(R.id.intro_indicator_3);
        imageViews = new ImageView[]{zero, one, two};

        //call adapter
         pagerAdapter = new SliderAdapter(this, display);
         viewPager.setAdapter(pagerAdapter);
         sharedPreferences = this.getSharedPreferences("Radio_group", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if(viewPager.getCurrentItem() == 0)
        {
            editor.putInt("Position", 0);
            editor.apply();
        }
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @SuppressLint("UseCompatTextViewDrawableApis")
            @Override
            public void onPageSelected(int position) {
                editor.putInt("Position", position);
                editor.apply();
                updateIndicators(position);
                if (position == 2){
                    button.setVisibility(View.VISIBLE);
                    button.setCompoundDrawableTintList(ColorStateList.valueOf(Color.parseColor("#4D000000")));
                    button.setOnClickListener(v -> {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    });
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
//

    }

    void updateIndicators(int position) {
        for (int i = 0; i < imageViews.length; i++) {
            imageViews[i].setBackgroundResource(
                    i == position ? R.drawable.indicator_selected : R.drawable.indicator_unselected
            );
        }
    }

}