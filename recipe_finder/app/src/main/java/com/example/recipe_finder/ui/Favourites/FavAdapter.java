package com.example.recipe_finder.ui.Favourites;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recipe_finder.DB.FavListItem;
import com.example.recipe_finder.MainActivity;
import com.example.recipe_finder.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public class FavAdapter extends RecyclerView.Adapter<FavAdapter.ViewHolder> {

    public interface ItemClickListener {
        public void onItemClicked(FavListItem favListItem);
    }

    private FavAdapter.ItemClickListener listener;

    private List<FavListItem> favListItems;
    Context context;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView imageView;
        TextView textView;
        Button fav_button;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.recipe_item_image);
            textView = itemView.findViewById(R.id.recipe_item_text);
            fav_button = itemView.findViewById(R.id.recipe_item_button);
            imageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            FavAdapter.this.onItemClicked(this.getLayoutPosition());
        }
    }

    public FavAdapter(List<FavListItem> favListItems, Context context){
        this.favListItems = favListItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recipe_item,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        FavListItem favListItem = favListItems.get(position);
        try {
            viewHolder.imageView.setImageBitmap(BitmapFactory.decodeStream(context.openFileInput(favListItem.getName())));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        viewHolder.textView.setText(favListItem.getName());
        viewHolder.fav_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.appDB.favDao().delete(favListItem);
                File dir = context.getFilesDir();
                File file = new File(dir, favListItem.getName());
                boolean delete = file.delete();
                favListItems.remove(favListItem);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return favListItems.size();
    }

    public void setItemClickListener(FavAdapter.ItemClickListener listener) {
        this.listener = listener;
    }

    private void onItemClicked(int index) {
        if(this.listener != null){
            this.listener.onItemClicked(this.favListItems.get(index));
        }
    }


}
