package com.example.recipe_finder.ui.dashboard;

import org.jetbrains.annotations.NotNull;

public class RowItem {
    private String question;
    private String answer;

    public RowItem(String question, String answer) {
        this.question = question;
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }
    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @NotNull
    @Override
    public String toString() {
        return question + "\n" + answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
}
