package com.example.recipe_finder.DB;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface Ingredients_List_Dao {

    @Insert
    public void addData(IngredientsListItem ingredientsListItem);

    @Query("select * from ingredients_list")
    public List<IngredientsListItem> getIngredientsData();

    @Delete
    public void delete(IngredientsListItem ingredientsListItem);

    @Update
    public void update(IngredientsListItem ingredientsListItem);

    @Query("SELECT EXISTS (SELECT 1 FROM ingredients_list WHERE ingredient_name=:id)")
    public int count(String id);

}
