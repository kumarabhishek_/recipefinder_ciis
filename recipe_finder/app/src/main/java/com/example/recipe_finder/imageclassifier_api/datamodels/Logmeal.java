package com.example.recipe_finder.imageclassifier_api.datamodels;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Logmeal {

    @SerializedName("foodFamily")
    @Expose
    private List<FoodFamily> foodFamily = null;
    @SerializedName("foodType")
    @Expose
    private List<FoodType> foodType = null;
    @SerializedName("imageId")
    @Expose
    private Integer imageId;
    @SerializedName("model_versions")
    @Expose
    private ModelVersions modelVersions;
    @SerializedName("recognition_results")
    @Expose
    private List<RecognitionResult> recognitionResults = null;

    public List<FoodFamily> getFoodFamily() {
        return foodFamily;
    }

    public void setFoodFamily(List<FoodFamily> foodFamily) {
        this.foodFamily = foodFamily;
    }

    public List<FoodType> getFoodType() {
        return foodType;
    }

    public void setFoodType(List<FoodType> foodType) {
        this.foodType = foodType;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public ModelVersions getModelVersions() {
        return modelVersions;
    }

    public void setModelVersions(ModelVersions modelVersions) {
        this.modelVersions = modelVersions;
    }

    public List<RecognitionResult> getRecognitionResults() {
        return recognitionResults;
    }

    public void setRecognitionResults(List<RecognitionResult> recognitionResults) {
        this.recognitionResults = recognitionResults;
    }

}