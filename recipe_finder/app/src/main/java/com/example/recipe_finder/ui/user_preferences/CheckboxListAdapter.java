package com.example.recipe_finder.ui.user_preferences;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.example.recipe_finder.R;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class CheckboxListAdapter extends ArrayAdapter<Checkbox_Item> {

    Context context;
    int page_number;
    boolean[] checked_array;
    Set<String> selections;
    private int row_index = -1;

    public CheckboxListAdapter(@NonNull Context context,
                               int resource,
                               List<Checkbox_Item> items,
                               int page_number) {
        super(context, resource, items);
        this.page_number = page_number;
        this.context = context;
        this.checked_array = new boolean[items.size()];
        this.selections = new TreeSet<>();
    }

    /*private view holder class*/
    private class ViewHolder {
       CheckBox checkBox;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        CheckboxListAdapter.ViewHolder holder = null;
        Checkbox_Item checkbox_item = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.preferences_checkboxes, null);
            holder = new ViewHolder();
            holder.checkBox = convertView.findViewById(R.id.checkBox);
            convertView.setTag(holder);
        } else
            holder = (CheckboxListAdapter.ViewHolder) convertView.getTag();

        holder.checkBox.setText(checkbox_item.getCheckbox_text());

        if(page_number !=2) {
            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    SharedPreferences sharedPreferences;
                    if (cb.isChecked()) {
                        selections.add(cb.getText().toString());
                        checked_array[position] = true;
                    } else {
                        selections.remove(cb.getText().toString());
                        checked_array[position] = false;
                    }

                    sharedPreferences = context.getSharedPreferences("Selections", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("selection_" + page_number, String.join(", ", selections));
                    editor.apply();
                }
            });

            holder.checkBox.setChecked(checked_array[position]);
        }
        else{
            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    String selection;
                    if(cb.isChecked()) {
                        row_index = position;
                        selection = cb.getText().toString();
                    }
                    else {
                        row_index = -1;
                        selection = null;
                    }
                    notifyDataSetChanged();
                    SharedPreferences sharedPreferences = context.getSharedPreferences("Selections", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("selection_" + page_number, selection);
                    editor.apply();
                }
            });

            holder.checkBox.setChecked(row_index == position);

        }
        return convertView;
    }

}
