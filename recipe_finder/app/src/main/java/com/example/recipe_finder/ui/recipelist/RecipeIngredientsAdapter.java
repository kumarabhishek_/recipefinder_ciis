package com.example.recipe_finder.ui.recipelist;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recipe_finder.R;
import com.example.recipe_finder.model.ExtendedIngredient;


import java.util.List;

public class RecipeIngredientsAdapter extends RecyclerView.Adapter<RecipeIngredientsAdapter.RecipeIngredientsViewHolder> {

    private final LayoutInflater inflater;
    private List<ExtendedIngredient> ingredients;
    private float multiplicator = 1;


    public RecipeIngredientsAdapter(Context context, List<ExtendedIngredient> ingredients) {
        inflater = LayoutInflater.from(context);
        this.ingredients = ingredients;
    }

    @NonNull
    @Override
    public RecipeIngredientsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.recipe_ingredient_item, parent, false);
        return new RecipeIngredientsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeIngredientsViewHolder holder, int position) {
        if (ingredients != null){
            ExtendedIngredient current = ingredients.get(position);
            holder.ingredientText.setText(current.getMeasures().getUs().getUnitShort() + " " + current.getName());
            holder.amount.setText(String.valueOf(current.getMeasures().getUs().getAmount() * multiplicator));
        }
    }

    @Override
    public int getItemCount() {
        if(ingredients != null)
            return ingredients.size();
        return 0;
    }

    public void updateIngredientAmount(int originalServings, float customServings) {
        multiplicator = customServings / originalServings;
        notifyDataSetChanged();

    }

    public class RecipeIngredientsViewHolder extends RecyclerView.ViewHolder {
        private final TextView amount;
        private final TextView ingredientText;

        public RecipeIngredientsViewHolder(@NonNull View itemView) {
            super(itemView);
            ingredientText = itemView.findViewById(R.id.recipe_ingredient);
            amount = itemView.findViewById(R.id.recipe_ingredient_amount);
        }

    }

}

