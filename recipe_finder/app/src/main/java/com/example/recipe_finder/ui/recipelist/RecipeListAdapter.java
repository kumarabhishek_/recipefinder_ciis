
package com.example.recipe_finder.ui.recipelist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recipe_finder.R;
import com.example.recipe_finder.model.Recipe;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecipeListAdapter extends RecyclerView.Adapter<RecipeListAdapter.RecipeViewHolder> {

    public interface ItemClickListener {
        public void onItemClicked(int index);
    }
    private ItemClickListener listener;

    public class RecipeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ImageView recipeImage;
        private final TextView recipeTitle;
        public RecipeViewHolder(@NonNull View itemView) {
            super(itemView);
            recipeTitle = itemView.findViewById(R.id.recipe_item_text);
            recipeImage = itemView.findViewById(R.id.recipe_item_image);
            recipeImage.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            RecipeListAdapter.this.onItemClicked(this.getLayoutPosition());
        }

    }

    private final LayoutInflater inflater;
    private List<Recipe> recipes;

    public RecipeListAdapter(Context context){
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.recipe_item, parent, false);

        //remove favorites button
        Button button = itemView.findViewById(R.id.recipe_item_button);
        button.setVisibility(View.GONE);

        return new RecipeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeViewHolder holder, int position) {
        if (recipes != null){
            Recipe current = recipes.get(position);
            holder.recipeTitle.setText(current.getTitle());
            Picasso.get().load(current.getImage()).into(holder.recipeImage);
        }
    }

    public void setRecipes(List<Recipe> recipes){
        this.recipes = recipes;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (recipes != null)
            return recipes.size();
        else return 0;
    }

    public void setItemClickListener(ItemClickListener listener) {
        this.listener = listener;
    }

    private void onItemClicked(int index) {
        if(this.listener != null){
            this.listener.onItemClicked(index);
        }
    }

}

