package com.example.recipe_finder.imageclassifier_api.datamodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelVersions {

    @SerializedName("foodType")
    @Expose
    private String foodType;
    @SerializedName("foodgroups")
    @Expose
    private String foodgroups;
    @SerializedName("foodrec")
    @Expose
    private String foodrec;
    @SerializedName("ingredients")
    @Expose
    private String ingredients;

    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public String getFoodgroups() {
        return foodgroups;
    }

    public void setFoodgroups(String foodgroups) {
        this.foodgroups = foodgroups;
    }

    public String getFoodrec() {
        return foodrec;
    }

    public void setFoodrec(String foodrec) {
        this.foodrec = foodrec;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

}