package com.example.recipe_finder.ui.recipelist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recipe_finder.R;
import com.example.recipe_finder.model.Step;


import java.util.List;

public class InstructionsAdapter extends RecyclerView.Adapter<InstructionsAdapter.InstructionsViewHolder> {

    private final LayoutInflater inflater;
    private List<Step> steps;


    public InstructionsAdapter(Context context, List<Step> steps) {
        inflater = LayoutInflater.from(context);
        this.steps = steps;
    }

    @NonNull
    @Override
    public InstructionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.recipe_instruction_item, parent, false);
        return new InstructionsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull InstructionsViewHolder holder, int position) {
        if (steps != null){
            Step current = steps.get(position);
            holder.stepNumber.setText(current.getNumber().toString() + '.');
            holder.stepText.setText(current.getStep());
        }
    }

    @Override
    public int getItemCount() {
        if(steps != null)
            return steps.size();
        return 0;
    }

    public class InstructionsViewHolder extends RecyclerView.ViewHolder {
        private final TextView stepNumber;
        private final TextView stepText;

        public InstructionsViewHolder(@NonNull View itemView) {
            super(itemView);
            stepNumber = itemView.findViewById(R.id.instruction_number);
            stepText = itemView.findViewById(R.id.instruction_text);
        }

    }

}

