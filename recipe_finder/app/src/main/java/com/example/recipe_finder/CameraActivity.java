package com.example.recipe_finder;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.Preview;
import androidx.camera.extensions.HdrImageCaptureExtender;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;

import com.example.recipe_finder.imageclassifier_api.api_utils.APIUtils;
import com.example.recipe_finder.imageclassifier_api.api_utils.LogmealAPIService;
import com.example.recipe_finder.imageclassifier_api.datamodels.Logmeal;
import com.example.recipe_finder.imageclassifier_api.datamodels.RecognitionResult;
import com.example.recipe_finder.ml.Model;
import com.example.recipe_finder.ui.home.HomeFragment;
import com.google.common.util.concurrent.ListenableFuture;

import org.tensorflow.lite.support.image.TensorImage;
import org.tensorflow.lite.support.label.Category;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CameraActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private final Executor executor = Executors.newSingleThreadExecutor();

    PreviewView mPreviewView;
    ImageView captureImage;
    int request_code = 999;
    private static final String[] CAMERA_PERMISSION = new String[]{Manifest.permission.CAMERA};
    private static final int CAMERA_REQUEST_CODE = 10;

    private LogmealAPIService apiService;
    List<RecognitionResult> itemLabels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        mPreviewView = findViewById(R.id.camera_texture_view);
        captureImage = findViewById(R.id.capture_image);

        if (hasCameraPermission()) {
                    startCamera();
                } else {
                    requestPermission();
                }
    }

    private boolean hasCameraPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, CAMERA_PERMISSION, CAMERA_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
                startCamera();
        }
        else
            finish();
    }

    private void startCamera() {

        final ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);

        cameraProviderFuture.addListener(() -> {
            try {

                ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                bindPreview(cameraProvider);

            } catch (ExecutionException | InterruptedException e) {
                // No errors need to be handled for this Future.
                // This should never be reached.
            }
        }, ContextCompat.getMainExecutor(this));
    }

    void bindPreview(@NonNull ProcessCameraProvider cameraProvider) {

        Preview preview = new Preview.Builder().build();
        CameraSelector cameraSelector = new CameraSelector.Builder().requireLensFacing(CameraSelector.LENS_FACING_BACK).build();
        ImageAnalysis imageAnalysis = new ImageAnalysis.Builder().build();
        ImageCapture.Builder builder = new ImageCapture.Builder();

        //Vendor-Extensions (The CameraX extensions dependency in build.gradle)
        HdrImageCaptureExtender hdrImageCaptureExtender = HdrImageCaptureExtender.create(builder);

        // Query if extension is available (optional).
        if (hdrImageCaptureExtender.isExtensionAvailable(cameraSelector)) {
            // Enable the extension if available.
            hdrImageCaptureExtender.enableExtension(cameraSelector);
        }

        final ImageCapture imageCapture = builder
                //.setTargetRotation(this.getWindowManager().getDefaultDisplay().getRotation())
                // .setTargetRotation(Surface.ROTATION_0)
                .build();
        preview.setSurfaceProvider(mPreviewView.getSurfaceProvider());
        Camera camera = cameraProvider.bindToLifecycle((LifecycleOwner)this, cameraSelector, preview, imageAnalysis, imageCapture);

        captureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                imageCapture.takePicture(executor, new ImageCapture.OnImageCapturedCallback() {
                    @Override
                    public void onError(@NonNull ImageCaptureException exception) {
                        super.onError(exception);
                        Context context = getApplicationContext();
                        CharSequence text = "Image capture failed";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }

                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onCaptureSuccess(@NonNull ImageProxy image) {
                        super.onCaptureSuccess(image);
                        String file_name = "Latest_photo";
                        try{
                            Bitmap bitmap = imageProxyToBitmap(image);
                            int angle = image.getImageInfo().getRotationDegrees();
                            bitmap = rotateImage(bitmap, angle);
                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                            FileOutputStream fo = openFileOutput(file_name, Context.MODE_PRIVATE);

                            fo.write(bytes.toByteArray());
                            fo.close();
                            image.close();
                            // Converting the image into JPEG format and saving it for Logmeal API
                            String path = getApplicationContext().getExternalFilesDir(null).toString();
                            Log.v("Photosaving", path);
                            File f = new File(path, "latest_foodItem.jpg");
                            FileOutputStream fos = new FileOutputStream(f);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                            fos.flush();
                            fos.close();

                            try {
                                Model model = Model.newInstance(getApplicationContext());

                                // Creates inputs for reference.
                                TensorImage input = TensorImage.fromBitmap(bitmap);

                                // Runs model inference and gets result.
                                Model.Outputs outputs = model.process(input);
                                List<Category> probability = outputs.getProbabilityAsCategoryList();
                                probability.sort(Comparator.comparing(Category::getScore));

                                Intent intent = new Intent(CameraActivity.this, SelectLabels.class);

                                //Logmeal API call
                                apiService = APIUtils.getAPIService();
                                RequestBody requestFile =
                                        RequestBody.create(
                                                MediaType.parse("multipart/form-data"),
                                                f
                                        );
                                MultipartBody.Part body =
                                        MultipartBody.Part.createFormData("image", f.getName(), requestFile);
                                Call<Logmeal> call = apiService.uploadImage(body);
//                                Toast.makeText(getApplicationContext(), "Waiting for prediction", Toast.LENGTH_SHORT).show();
                                call.enqueue(new Callback<Logmeal>() {
                                    @Override
                                    public void onResponse(Call<Logmeal> call,
                                                           Response<Logmeal> response) {

                                        //If API call is successful, label_1 is from Logmeal and rest from machine learning model
                                        if(response.code() == 200 && response.body().getRecognitionResults() != null) {
                                            itemLabels = response.body().getRecognitionResults();
                                            Log.v("Logmeal Upload", "Upload success. Predictions: "+response.body().getRecognitionResults().get(0).getName());
                                            intent.putExtra("label_1", itemLabels.get(0).getName());
                                        }
                                        else {
                                            Log.v("Logmeal Upload", "Upload success, but error with prediction" );
                                            intent.putExtra("label_1", probability.get(probability.size()-4).getLabel());
                                        }

                                        //Rest of the item labels comes from the model
                                        intent.putExtra("label_2", probability.get(probability.size()-1).getLabel());
                                        intent.putExtra("label_3", probability.get(probability.size()-2).getLabel());
                                        intent.putExtra("label_4", probability.get(probability.size()-3).getLabel());
                                        startActivityForResult(intent, request_code);

                                        // Releases model resources if no longer used.
                                        model.close();
                                    }

                                    @Override
                                    public void onFailure(Call<Logmeal> call, Throwable t) {
                                        Log.e("Logmeal Upload error:", t.getMessage());
                                    }
                                });
                                //End of Logmeal

                            } catch (IOException e) {
                                // TODO Handle the exception
                            }
                        }
                        catch(Exception e)
                        {
                            e.printStackTrace();
                        }

                    }
                });

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Intent intent = new Intent(this, HomeFragment.class);
            intent.putExtra("Label", data.getStringExtra("Final_label"));
            setResult(RESULT_OK, intent);
            finish();
        }

    }

    private Bitmap imageProxyToBitmap(ImageProxy image)
    {
        ImageProxy.PlaneProxy planeProxy = image.getPlanes()[0];
        ByteBuffer buffer = planeProxy.getBuffer();
        byte[] bytes = new byte[buffer.remaining()];
        buffer.get(bytes);

        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }
}
