package com.example.recipe_finder;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileNotFoundException;

public class SelectLabels extends AppCompatActivity {

    ImageView imageView;
    EditText editText;
    TextView[] textViews;
    String final_label;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_select_labels);
        imageView = findViewById(R.id.latest_photo_select_label);
        textViews = new TextView[4];
        textViews[0] = findViewById(R.id.label_1);
        textViews[1] = findViewById(R.id.label_2);
        textViews[2] = findViewById(R.id.label_3);
        textViews[3] = findViewById(R.id.label_4);
        editText = findViewById(R.id.write_label);

        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(this.openFileInput("Latest_photo"));
            imageView.setImageBitmap(bitmap);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Intent i = getIntent();

        textViews[0].setText(i.getStringExtra("label_1"));
        textViews[1].setText(i.getStringExtra("label_2"));
        textViews[2].setText(i.getStringExtra("label_3"));
        textViews[3].setText(i.getStringExtra("label_4"));

        Intent return_intent = new Intent(SelectLabels.this, CameraActivity.class);

        for(int j = 0; j< 4; j++)
        {
            textViews[j].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                            TextView temp = (TextView) v;
                            final_label = temp.getText().toString();
                            Intent return_intent = new Intent(SelectLabels.this, CameraActivity.class);
                            return_intent.putExtra("Final_label", final_label);
                            setResult(RESULT_OK, return_intent);
                            finish();
                }
            });
        }

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    final_label = v.getText().toString();
                    return_intent.putExtra("Final_label", final_label);
                    setResult(RESULT_OK, return_intent);
                    finish();
                    return  true;
                }

                return false;
            }
        });


    }

}