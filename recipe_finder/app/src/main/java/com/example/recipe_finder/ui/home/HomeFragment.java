package com.example.recipe_finder.ui.home;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recipe_finder.CameraActivity;
import com.example.recipe_finder.CameraActivity_new;
import com.example.recipe_finder.DB.IngredientsListItem;
import com.example.recipe_finder.MainActivity;
import com.example.recipe_finder.R;
import com.example.recipe_finder.RecipeListActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class HomeFragment extends Fragment {

    private Animation rotateOpen, rotateClose, translateOpen, translateClose;
    private FloatingActionButton add, write, camera;
    ImageView latest_photo;
    Bitmap bitmap;
    Boolean isOpen = false;
    CardView cardView;
    private RecyclerView.Adapter adapter;
    LinearLayoutManager linearLayoutManager;
    int requestCode_ = 10;
    List<IngredientsListItem> ingredientsListItems;



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        latest_photo = root.findViewById(R.id.latest_photo);
        RecyclerView ingredient_list_view = root.findViewById(R.id.ingredients_list);
        ingredient_list_view.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getContext());
        ingredient_list_view.setLayoutManager(linearLayoutManager);

        Display display = getActivity().getWindowManager().getDefaultDisplay();

        ingredientsListItems = MainActivity.appDB.ingredients_list_dao().getIngredientsData();
        cardView = root.findViewById(R.id.card_view_findrecipes);
        add = root.findViewById(R.id.floating_add);
        write = root.findViewById(R.id.floating_write);
        camera = root.findViewById(R.id.floating_camera);

        cardView.getLayoutParams().height = display.getWidth()/2;
        cardView.getLayoutParams().width = display.getWidth()/2;
        cardView.setRadius((float) display.getWidth()/4);

        Button findRecipesButton = root.findViewById(R.id.find_recipes);
        // findRecipesButton.getLayoutParams().width = display.getWidth()/2;

        rotateOpen = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_open_anim);
        rotateClose = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_close_anim);
        translateOpen = AnimationUtils.loadAnimation(getContext(), R.anim.from_bottom_anim);
        translateClose = AnimationUtils.loadAnimation(getContext(), R.anim.to_bottom_anim);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOpen) {
                    write.setVisibility(View.INVISIBLE);
                    camera.setVisibility(View.INVISIBLE);
                    add.setAnimation(rotateClose);
                    write.setAnimation(translateClose);
                    camera.setAnimation(translateClose);
                    write.setClickable(false);
                    camera.setClickable(false);
                    isOpen = false;
                } else {
                    write.setVisibility(View.VISIBLE);
                    camera.setVisibility(View.VISIBLE);
                    add.setAnimation(rotateOpen);
                    write.setAnimation(translateOpen);
                    camera.setAnimation(translateOpen);
                    write.setClickable(true);
                    camera.setClickable(true);
                    isOpen = true;
                }
            }
        });

        adapter = new IngredientListAdapter(ingredientsListItems, getContext());
        ingredient_list_view.setAdapter(adapter);

        write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater dialog_inflater = LayoutInflater.from(getContext());
                View write_dialog = dialog_inflater.inflate(R.layout.write_dialog, null);
                AlertDialog.Builder write_dialog_builder = new AlertDialog.Builder(requireContext(), R.style.AlertDialogCustom);
                write_dialog_builder.setView(write_dialog);

                final EditText userInputDialogEditText = write_dialog.findViewById(R.id.write_dialog_edittext);
                write_dialog_builder.setCancelable(false).setPositiveButton("Add",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                            }
                        })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                });

                AlertDialog alertDialogAndroid = write_dialog_builder.create();
                alertDialogAndroid.show();
                alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(MainActivity.appDB.ingredients_list_dao().count(userInputDialogEditText.getText().toString()) > 0)
                            userInputDialogEditText.setError("This already exists in the list. Click \"Cancel\" to go back");
                        else {
                            IngredientsListItem ingredientsListItem = new IngredientsListItem();
                            ingredientsListItem.setIngredient_name(userInputDialogEditText.getText().toString());
                            ingredientsListItem.setIs_checked(true);
                            ingredientsListItems.add(ingredientsListItem);
                            adapter.notifyDataSetChanged();
                            MainActivity.appDB.ingredients_list_dao().addData(ingredientsListItem);
                            alertDialogAndroid.dismiss();
                        }
                    }
                });
            }
        });


        camera.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), CameraActivity_new.class);
            startActivityForResult(intent, requestCode_);
        });

        findRecipesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), RecipeListActivity.class);

//              requires API level 26
//              String ingredientString = String.join(",", ingredients);

                List<String> ingredients = new ArrayList<>();
                for(IngredientsListItem ingredientsListItem : ingredientsListItems) {
                    if(ingredientsListItem.isIs_checked())
                        ingredients.add(ingredientsListItem.getIngredient_name());
                }
                //convert ingredient list to String for API search,
                StringBuilder csvBuilder = new StringBuilder();
                for(String ingredient : ingredients){
                    csvBuilder.append(ingredient);
                    csvBuilder.append(",");
                }
                String ingredientString = csvBuilder.toString();

                intent.putExtra("ingredients",ingredientString);
                startActivity(intent);
            }
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

                MainActivity.appDB.ingredients_list_dao().delete(ingredientsListItems.get(viewHolder.getAdapterPosition()));
                ingredientsListItems.remove(viewHolder.getAdapterPosition());
                adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
            }
        }).attachToRecyclerView(ingredient_list_view);

        return root;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            try {
                bitmap = BitmapFactory.decodeStream(getContext().openFileInput("Latest_photo"));
                latest_photo.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            IngredientsListItem ingredientsListItem = new IngredientsListItem();
            if (data != null) {
                ingredientsListItem.setIngredient_name(data.getStringExtra("Label"));
                ingredientsListItem.setIs_checked(true);
            }
            if(MainActivity.appDB.ingredients_list_dao().count(ingredientsListItem.getIngredient_name())!=1)
            {
                MainActivity.appDB.ingredients_list_dao().addData(ingredientsListItem);
                ingredientsListItems.add(ingredientsListItem);
                adapter.notifyDataSetChanged();

            }
            else{
                Toast.makeText(getContext(), "This ingredient already exists in your catalogue !", Toast.LENGTH_LONG).show();
            }


        }

    }


}