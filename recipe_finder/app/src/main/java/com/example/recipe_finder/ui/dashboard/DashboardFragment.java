package com.example.recipe_finder.ui.dashboard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.recipe_finder.R;
import com.example.recipe_finder.ui.user_preferences.Onboarding;

import java.util.ArrayList;
import java.util.List;

public class DashboardFragment extends Fragment {

    Button button;
    List<RowItem> rowItems;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        button = root.findViewById(R.id.preferences_change_button);
        ListView list_view = root.findViewById(R.id.preferences_list);

        int[] questions = {R.string.onboarding_2_title, R.string.onboarding_3_title, R.string.onboarding_4_title};
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("Selections", Context.MODE_PRIVATE);

        rowItems = new ArrayList<>();
        for(int i = 0; i<questions.length; i++)
        {
            RowItem item = new RowItem(getString(questions[i]),
                    sharedPreferences.getString("selection_" + i, ""));
            rowItems.add(item);
        }

        CustomListViewAdapter adapter = new CustomListViewAdapter(getContext(), R.layout.preferences_list_item, rowItems);
        list_view.setAdapter(adapter);

        button.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), Onboarding.class);
            startActivity(intent);
        });

        return root;
    }
}