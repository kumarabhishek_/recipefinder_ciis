package com.example.recipe_finder.ui.dashboard;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.recipe_finder.R;

import java.util.List;

public class CustomListViewAdapter extends ArrayAdapter<RowItem> {

    Context context;

    public CustomListViewAdapter(Context context, int resourceId,
                                 List<RowItem> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        TextView question;
        TextView answer;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        RowItem rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.preferences_list_item, null);
            holder = new ViewHolder();
            holder.question = (TextView) convertView.findViewById(R.id.preference_question);
            holder.answer = (TextView) convertView.findViewById(R.id.preference_answer);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.answer.setText(rowItem.getAnswer());
        holder.question.setText(rowItem.getQuestion());

        return convertView;
    }
}
