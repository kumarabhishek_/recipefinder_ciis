package com.example.recipe_finder.imageclassifier_api.api_utils;

public class APIUtils {
    private APIUtils() {}

    public static final String BASE_URL = "https://api.logmeal.es/v2/";

    public static LogmealAPIService getAPIService() {

        return Retrofit_logmeal.getClient(BASE_URL).create(LogmealAPIService.class);
    }
}
