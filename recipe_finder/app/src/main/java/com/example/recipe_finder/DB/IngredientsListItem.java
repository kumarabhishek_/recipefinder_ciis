package com.example.recipe_finder.DB;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

@Entity(tableName = "ingredients_list")
public class IngredientsListItem {
//    @PrimaryKey(autoGenerate = true)
//    private int id;

    @PrimaryKey@NonNull
    @ColumnInfo(name = "ingredient_name")
    private String ingredient_name;

    @ColumnInfo(name = "ingredient_check")
    private boolean is_checked;


    public boolean isIs_checked() {
        return is_checked;
    }

    public void setIs_checked(boolean is_checked) {
        this.is_checked = is_checked;
    }

    @NotNull
    public String getIngredient_name() {
        return ingredient_name;
    }

    public void setIngredient_name(@NotNull String ingredient_name) {
        this.ingredient_name = ingredient_name;
    }

}
