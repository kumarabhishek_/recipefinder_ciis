
# Guten Appetit

Have you ever gone to the kitchen and wondered what could be cooked from the raw food items available? 
Or have you ever wanted to know which recipe could be the quickest? 
Or, ever wished to know what some strange looking fruit or vegetable at a supermarket is?

Guten Appetit is a solution that addresses these 3 questions in 3 easy steps: 

1. Fill the user taste preference questionnaire
2. Take picture of the food/vegetables/ingredients available and choose one out of 4 predictions done by our machine learning model. The ingredients could also be manually typed-in
3. Get a list of lip-smacking recipe suggestions based on the user-preferences entered

In addition to these, It also allows the user to have a digital copy of all the ingredients/food items they own.

## Installation

Clone the repository and import into ***Android Studio*** 
```bash
git clone https://bitbucket.org/kumarabhishek_/recipefinder_ciis.git
```



## Generating APK
From Android Studio:

1. ***Build*** menu
2. Select ***Build Bundle(s)/APK(s)*** > ***Build APK(s)***


## Authors
1. Naga Sai Surya Vamsy Malladi
2. Maximo Schmidt
3. Kumar Abhishek