import os
import tensorflow as tf
from tensorflow import keras
import numpy as np


image_size = 128
batch_size = 32
IMG_SHAPE = (image_size, image_size, 3)

# Downloading Fruits360 dataset and splitting it to training and validation directory
zip_file = tf.keras.utils.get_file(origin="https://storage.googleapis.com/kaggle-datasets/5857/414958/fruits-360_dataset.zip?GoogleAccessId=web-data@kaggle-161607.iam.gserviceaccount.com&Expires=1566314394&Signature=rqm4aS%2FIxgWrNSJ84ccMfQGpjJzZ7gZ9WmsKok6quQMyKin14vJyHBMSjqXSHR%2B6isrN2POYfwXqAgibIkeAy%2FcB2OIrxTsBtCmKUQKuzqdGMnXiLUiSKw0XgKUfvYOTC%2F0gdWMv%2B2xMLMjZQ3CYwUHNnPoDRPm9GopyyA6kZO%2B0UpwB59uwhADNiDNdVgD3GPMVleo4hPdOBVHpaWl%2F%2B%2BPDkOmQdlcH6b%2F983JHaktssmnCu8f0LVeQjzZY96d24O4H85x8wdZtmkHZCoFiIgCCMU%2BKMMBAbTL66QiUUB%2FW%2FpULPlpzN9sBBUR2yydB3CUwqLmSjAcwz3wQ%2FpIhzg%3D%3D",
                                   fname="fruits-360.zip", extract=True)
base_dir, _ = os.path.splitext(zip_file)
train_dir = os.path.join(base_dir, 'Training')
validation_dir = os.path.join(base_dir, 'Test')
class_labels = os.listdir(train_dir)

# Training and Validation data generator
train_datagen = keras.preprocessing.image.ImageDataGenerator()
train_generator = train_datagen.flow_from_directory(directory=train_dir, target_size=(image_size, image_size), batch_size=batch_size)
validation_datagen = keras.preprocessing.image.ImageDataGenerator()
validation_generator = validation_datagen.flow_from_directory(directory=validation_dir, target_size=(image_size, image_size), batch_size=batch_size)


# Importing the MobileNet model
base_model = tf.keras.applications.MobileNet(input_shape=IMG_SHAPE, include_top=False)
base_model.trainable = False

# Adding second last layer and a Dense layer
base_model = tf.keras.Sequential([
  base_model,
  keras.layers.GlobalAveragePooling2D(),
  keras.layers.Dense(131, activation='sigmoid')
])

base_model.summary()

# Compiling the model
base_model.compile(optimizer=tf.keras.optimizers.RMSprop(lr=0.0001),
              loss='binary_crossentropy',
              metrics=['accuracy'])

epochs = 25
steps_per_epoch = numpy.ceil(train_generator.n / batch_size)
validation_steps = numpy.ceil(validation_generator.n / batch_size)

# Training the model
history = base_model.fit_generator(generator=train_generator,
                              steps_per_epoch = steps_per_epoch,
                              epochs=epochs,
                              validation_data=validation_generator,
                              validation_steps=validation_steps)

base_model.save('MobileNet_TransferLearning_Fruits360v48.h5')


# To convert Tensorflow model to tflite
converter = tf.lite.TFLiteConverter.from_keras_model(base_model)
tflite_model = converter.convert()
open("model.tflite", "wb").write(tflite_model)
output = np.savetxt("labels.txt", class_labels, delimiter=', ', fmt="%s")
